const getCube = 10 ** 3;
console.log(`The cube of 10 is ${getCube}`);


let address = [182, "Ilang-Ilang St.", "Palahanan", "Balete", "Batangas" ];

let [houseNumber, street, barangay, city, province] = address;
console.log(`I live at ${houseNumber} ${street}, ${barangay}, ${city}, ${province}`);


let animal = {
	name: "Bagwis",
	kind: "Philippine Eagle",
	age: "2y/o",
	size: "86-102cm",
	weight:"5-7kg"
};

let {name, kind, age, size, weight} = animal;
console.log(`${name} is a ${age} ${kind}. He weighed at ${weight} with a measurement of ${size}.`);

let arrayOfNumbers = [2, 4, 6, 8, 10];

arrayOfNumbers.forEach((number) =>{
	console.log(`${number}`);
});

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

const myDog = new Dog("Theo", "1", "Shih Tzu");
console.log(myDog);

